import React, { useState, useEffect } from "react";
import "./chat.css";
import * as config from "./config";
import Spinner from "../spinner/";
import Header from "../header";
import MessageList from "../message-list";
import MessageInput from "../message-input";
import { getAllMessages } from "../../services/messageService";

function Chat() {
  const [data, setMessages] = useState([]);
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    async function fetchData() {
      const response = await getAllMessages();
      setMessages(response);
    }
    fetchData();
    setLoaded(true);
  }, []);

  const addPost = (body) => {
    const newPost = {
      id: data.length + 1,
      userId: config.CURRENT_USER_ID,
      avatar: config.CURRENT_USER_AVATAR,
      user: config.CURRENT_USER_NAME,
      text: body,
      createdAt: new Date(Date.now()).toISOString(),
      editedAt: "",
    };
    const newState = [...data, newPost];
    setMessages(newState);
  };
  const deletePost=(postId)=>{
    setMessages(data.filter(post => post.id !== postId))
  }
  const nParticipants = (state) => {
    const set = new Set();
    state.forEach((element) => {
      set.add(element.userId);
    });
    return set.size;
  };
  const lastMessageTime = (state) => {
    const createdAts = state.map((elem) => Date.parse(elem.createdAt));
    const max = Math.max(...createdAts);
    return new Date(max).toLocaleTimeString().slice(0, -3);
  };

  return (
    <div className="chat">
      {!isLoaded && <Spinner />}
      {isLoaded && (
        <>
          <Header
            name={config.CHAT_NAME}
            nParticipants={nParticipants(data)}
            nMessages={data.length}
            lastMessage={lastMessageTime(data)}
          />
          <MessageList data={data} currentUserId={config.CURRENT_USER_ID} deletePost={deletePost}/>
          <MessageInput addPost={addPost} />
        </>
      )}
    </div>
  );
}

export default Chat;
