import React from "react";
import moment from "moment";

import "./my-own-post.css";

const MyOwnPost = ({ post, deletePost }) => {
  const { user, text, createdAt, id } = post;
  const date = moment(createdAt).fromNow();
  return (
    <div className="myown-card">
      <div className="card-content">
        <div className="card-content-header">{user}</div>
        <div className="card-content-meta">posted {date}</div>
        <div className="card-description">
          <strong>{text}</strong>
        </div>
      </div>
      <div className="card-extra-content">
          <span><i className="fa fa-pencil"></i></span>
          <span><i className="fas fa-trash-alt" onClick={()=>deletePost(id)}></i></span>
      </div>
    </div>
  );
};

export default MyOwnPost;