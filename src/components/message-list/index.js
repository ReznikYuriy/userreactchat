import React from "react";
import "./message-list.css";
import Post from "../Post";
import MyOwnPost from "../MyOwnPost";

const MessageList = ({ data, currentUserId, deletePost }) => {
  return (
    <div className="message-list">
      {data.map((post) =>
        (post.userId !== currentUserId) ? (
          <Post key={post.id} post={post} currentUserId={currentUserId} />
        ) : (
          <MyOwnPost key={post.id} post={post} currentUserId={currentUserId} deletePost={deletePost}/>
        )
      )}
    </div>
  );
};

export default MessageList;
