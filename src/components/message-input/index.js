import React, { useState } from "react";
import "./message-input.css";

const MessageInput = ({ addPost }) => {
  const [body, setBody] = useState("");

  const handleAddPost = (ev) => {
    if (!body) {
      return;
    }
    addPost(body);
    setBody("");
    ev.preventDefault();
  };

  return (
    <div className="message-input">
      <form onSubmit={handleAddPost}>        
      <textarea
          name="description"
          value={body}
          placeholder="What is the news?"
          onChange={(ev) => setBody(ev.target.value)}
        />

        <button className="submit-button" type="submit">
          Post
        </button>
      </form>
    </div>
  );
};

export default MessageInput;
