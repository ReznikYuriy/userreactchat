import React, {useState} from "react";
import moment from "moment";

import "./post.css";

const Post = ({ post }) => {
  const { avatar, user, text, createdAt } = post;
  const [isLiked, setLiked]=useState("up");
  const date = moment(createdAt).fromNow();
  const toggleLike=()=>{
    (isLiked==='up') ? setLiked('down') : setLiked('up');
  }
  return (
    <div className="default-card">
      <div className="card-content">
        <img className="card-img" size="mini" src={avatar} alt="avatar" />
        <div className="card-content-header">{user}</div>
        <div className="card-content-meta">posted {date}</div>
        <div className="card-description">
          <strong>{text}</strong>
        </div>
      </div>
      <div className="card-extra-content">
          <span><i className={`thumbs ${isLiked} icon`} onClick={toggleLike}></i></span>
      </div>
    </div>
  );
};

export default Post;
