import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './components/Chat';
import 'semantic-ui-css/semantic.min.css';

ReactDOM.render(
    <Chat />,
  document.getElementById('root')
);