export const addClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
  };
  
  export const removeClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.remove(...classNames);
  };
  
  export const formatClassNames = className => className.split(" ").filter(Boolean);
  